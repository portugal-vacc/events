from datetime import date

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from events.models import Event


class EventDateListFilter(admin.SimpleListFilter):
    title = _('Event Date')

    parameter_name = 'date'

    def lookups(self, request, model_admin):
        return (
            ('Future', _('In the future')),
            ('Past', _('In the past')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'Future':
            return queryset.filter(start_time__gte=date.today())
        if self.value() == 'Past':
            return queryset.filter(start_time__lte=date.today())


class EventAdmin(admin.ModelAdmin):
    list_filter = (EventDateListFilter,)
    date_hierarchy = 'start_time'


admin.site.register(Event, EventAdmin)
