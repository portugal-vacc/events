from typing import List, Optional

import httpx
from pydantic import ValidationError

from controller.exceptions import ExternalServiceClientErrorResponse, ExternalServiceServerErrorResponse, \
    ExternalServiceTypeVerificationError
from events.schemas import EventSchema


def get_request(url: str) -> dict:
    response = httpx.get(url)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse()
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse()
    return response.json()


def get_all_events() -> List[Optional[EventSchema]]:
    data = get_request('https://my.vatsim.net/api/v1/events/all')
    events = data['data']
    try:
        return [EventSchema(**event) for event in events]
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


def parse_portugal_events(all_events: List[Optional[EventSchema]]) -> List[Optional[EventSchema]]:
    _portugal_events: List = []
    for event in all_events:
        if [airport for airport in event.airports if airport.icao.startswith("LP")]:
            _portugal_events.append(event)
    return _portugal_events


def portugal_events() -> List[Optional[EventSchema]]:
    return parse_portugal_events(get_all_events())
