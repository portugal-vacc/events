from ninja import NinjaAPI

from controller.exceptions import ExternalServiceServerErrorResponse, ExternalServiceClientErrorResponse, \
    ExternalServiceTypeVerificationError, DualBookingError, MinimumBookingTimeError

my_api = NinjaAPI(title="Portugal vACC API", version="1.0.0",
                  description="Portugal vACC Application Programming Interface")


@my_api.exception_handler(ExternalServiceServerErrorResponse)
def external_service_unavailable(request, exc):
    return my_api.create_response(
        request,
        {"message": "External Server is having issues. Please retry later", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(ExternalServiceClientErrorResponse)
def external_data_unavailable(request, exc):
    return my_api.create_response(
        request,
        {"message": "External Server can't find the info you're looking for...", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(ExternalServiceTypeVerificationError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "External data models changed... Ah shiet, here we go again", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(DualBookingError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "Booking is conflicting with another existing booking", "info": str(exc)},
        status=403,
    )


@my_api.exception_handler(MinimumBookingTimeError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "Booking should have minimum time 30 minutes", "info": str(exc)},
        status=403,
    )
